package com.smac89.weatherapp

import com.jfoenix.assets.JFoenixResources
import com.jfoenix.controls.JFXDecorator
import com.jfoenix.svg.SVGGlyph
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

class WeatherApp: Application() {
    override fun start(primaryStage: Stage) {
        val loader = FXMLLoader(javaClass.getResource("/weatherview.fxml"))
        val root: Parent = loader.load()

        val jfxDecorator = JFXDecorator(primaryStage, root, false, false, true)

        primaryStage.apply {
            scene = Scene(jfxDecorator).apply {
                stylesheets.addAll(
                    JFoenixResources.load("css/jfoenix-fonts.css").toExternalForm(),
                    JFoenixResources.load("css/jfoenix-design.css").toExternalForm()
                )
            }
            title = "Weather App FX"
            height = 640.0
            width = 480.0
            isResizable = false
        }.show()
    }

    companion object {
        @JvmStatic fun main(args: Array<String>) {
            launch(WeatherApp::class.java, *args)
        }
    }
}
