package com.smac89.weatherapp.internal

/**
 * Utility function to create PascalCase String
 * @receiver String This string
 * @return String The new PascalCase String
 */
fun String.capitalizeWords(): String = split(" ").joinToString(separator = " ", transform = String::capitalize)
