package com.smac89.weatherapp.service

import com.beust.klaxon.*
import java.net.URL

object OpenWeatherApi {

    var apiKeyProperty = "d5fb7a919324ad17ce1d7e8b45765b93"

    private val klaxon = Klaxon().converter(object: Converter {
        override fun canConvert(cls: Class<*>): Boolean = cls == WeatherInfo::class.java
        override fun fromJson(jv: JsonValue): Any? =
            WeatherInfo(
                jv.obj?.obj("main")?.get("temp").toString().toDouble(),
                jv.obj?.obj("main")?.get("feels_like").toString().toDouble(),
                jv.obj?.obj("wind")?.get("speed").toString().toDouble(),
                jv.obj?.array<JsonObject>("weather")?.get(0)?.string("description")!!,
                jv.obj?.array<JsonObject>("weather")?.get(0)?.string("icon")!!,
                jv.objString("name"),
                jv.obj?.obj("sys")?.string("country")!!
            )

        override fun toJson(value: Any): String = "{}"
    })
    private const val API_URL = "https://api.openweathermap.org/"

    fun getWeatherDataForCityID(cityId: String): WeatherInfo {
        return klaxon.parse(URL("${API_URL}/data/2.5/weather?units=metric&id=$cityId&appid=$apiKeyProperty")
            .openStream())!!
    }
}

data class WeatherInfo (@Json("$.main.temp") val temperature: Double,
                        @Json("$.main.feels_like") val feelsLike: Double,
                        @Json("$.wind.speed") val windSpeed: Double,
                        @Json("$.weather[0].description") val description: String,
                        @Json("$.weather[0].icon", ignored = false) private val _icon: String,
                        @Json("$.name", ignored = false) private val _cityName: String,
                        @Json("$.sys.country", ignored = false) private val _countryCode: String) {

    val icon = _icon
        get(): String = "https://openweathermap.org/img/wn/$field@2x.png"

    val locationName: String
        get() = "${_cityName.capitalize()}, ${_countryCode.toUpperCase()}"

    companion object {
        val EMPTY_WEATHER_INFO = WeatherInfo(Double.NaN, Double.NaN, Double.NaN,
            "clear sky", "03n", "Saskatoon", "CA")
    }
}
