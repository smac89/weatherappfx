package com.smac89.weatherapp.service

import com.beust.klaxon.*
import com.beust.klaxon.jackson.jackson
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import java.util.*

// TODO: Convert to using kotlin's coroutines
object CityIDSearchService {

    private val parser = Parser.jackson()

    private val cityList: List<City> by lazy {
        CityIDSearchService::class.java.getResourceAsStream("/city.list.json.gz").use {
            val gzipStream = GzipCompressorInputStream(it.buffered(1.shl(20)), true)
            Klaxon().converter(object: Converter {
                override fun canConvert(cls: Class<*>): Boolean = cls == City::class.java
                override fun fromJson(jv: JsonValue): Any? = City(
                    jv.obj?.int("id").toString(),
                    jv.objString("name"),
                    jv.objString("country"),
                    jv.obj?.obj("coord")?.get("lat").toString().toDouble(),
                    jv.obj?.obj("coord")?.get("lon").toString().toDouble()
                )
                override fun toJson(value: Any): String = "{}"

            }).parseFromJsonArray<City>(parser.parse(gzipStream) as JsonArray<*>)!!
        }
    }

    /**
     * Search for a city id
     * @param cityName String The name of the city
     * @param countryCode String The 2-letter country code
     */
    fun searchForCityID(cityName: String, countryCode: String): City {
        if (!Locale.getISOCountries().contains(countryCode.toUpperCase())) {
            throw IllegalArgumentException("The country code supplied is unknown: ${countryCode.toUpperCase()}")
        }

        return cityList.find {
            it.country == countryCode.toUpperCase() && cityName.capitalize() == it.name
        }!!
    }
}

/**
 * Simple Class representing a city
 * @property id String The city's id.
 * @property name String The name of the city
 * @property country String The 2-letter country code. See [[Locale.IsoCountryCode]]
 * @property lat Double The latitude of the city
 * @property lon Double The longitude of the city
 * @constructor
 */
data class City(
    val id: String,
    val name: String,
    val country: String,
    val lat: Double,
    val lon: Double
)
