package com.smac89.weatherapp.service

import com.beust.klaxon.Converter
import com.beust.klaxon.JsonValue
import com.beust.klaxon.Klaxon
import java.net.URL
import java.util.concurrent.*

object UserLocationService {
    /**
     * Attempts to locate a user based on their ip address
     * @return Future<UserLocation>
     */
    fun getUserLocation(): CompletableFuture<in UserLocation> = CompletableFuture.anyOf(
        CompletableFuture.supplyAsync(GeoIpStrategy::findUserLocation),
        CompletableFuture.supplyAsync(IpApiStrategy::findUserLocation)
    )
}

sealed class LocationStrategy {
    abstract fun findUserLocation(): UserLocation
}

// Uses the freegeoip api to get the user's location
object GeoIpStrategy : LocationStrategy() {
    private const val API_URL = "https://freegeoip.app"

    private val klaxon = Klaxon().converter(object : Converter {
        override fun canConvert(cls: Class<*>): Boolean = cls == UserLocation::class.java
        override fun fromJson(jv: JsonValue): Any? =
            UserLocation(
                jv.objString("country_code"),
                jv.objString("country_name"),
                jv.objString("city"),
                jv.obj?.double("latitude")!!,
                jv.obj?.double("longitude")!!
            )

        // Ignored
        override fun toJson(value: Any): String = "{}"
    })

    override fun findUserLocation(): UserLocation =
        klaxon.parse(URL("$API_URL/json").openStream().buffered())!!
}

/**
 * Uses ipapi.co to retrieve the user's location
 */
object IpApiStrategy : LocationStrategy() {
    private const val API_URL = "https://ipapi.co"

    private val klaxon = Klaxon().converter(object : Converter {
        override fun canConvert(cls: Class<*>): Boolean = cls == UserLocation::class.java
        override fun fromJson(jv: JsonValue): Any? =
            UserLocation(
                jv.objString("country"),
                jv.objString("country_name"),
                jv.objString("city"),
                jv.obj?.double("latitude")!!,
                jv.obj?.double("longitude")!!
            )

        // Ignored
        override fun toJson(value: Any): String = "{}"
    })

    override fun findUserLocation(): UserLocation =
        klaxon.parse(URL("$API_URL/json").openStream().buffered())!!
}

data class UserLocation(
    val countryCode: String,
    val country: String,
    val city: String,
    val latitude: Double,
    val longitude: Double
)
