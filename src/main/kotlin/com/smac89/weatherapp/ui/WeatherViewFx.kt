package com.smac89.weatherapp.ui

import com.jfoenix.controls.JFXTextField
import com.jfoenix.utils.JFXUtilities
import com.smac89.weatherapp.internal.capitalizeWords
import com.smac89.weatherapp.service.*
import javafx.animation.*
import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.MenuButton
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.KeyCode
import javafx.scene.layout.Pane
import javafx.util.Duration
import java.net.URL
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.concurrent.CompletableFuture
import kotlin.math.roundToInt

class WeatherViewFx {
    @FXML private lateinit var parentView: Pane
    @FXML private lateinit var searchView: Node
    @FXML private lateinit var loadingView: Node
    @FXML private lateinit var weatherView: Node

    @FXML private lateinit var locationSearchField: JFXTextField

    @FXML private lateinit var cityLabel: Label
    @FXML private lateinit var weatherDescription: Label
    @FXML private lateinit var weatherIcon: ImageView
    @FXML private lateinit var temperature: Label

    @FXML private lateinit var feelsLikeTemperature: Label
    @FXML private lateinit var windSpeed: Label

    @FXML private lateinit var updateInterval: MenuButton
    @FXML private lateinit var currentTime: Label

    private val timeFormatProperty = SimpleObjectProperty(this, "timeFormat",
        DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM))

    private val weatherInfoProperty = SimpleObjectProperty(this, "weatherInfo", WeatherInfo.EMPTY_WEATHER_INFO)

    private val searchAnimation by lazy { createViewTransitionAnimation(searchView) }
    private val loadingAnimation by lazy { createViewTransitionAnimation(loadingView) }

    private val currentTimeUpdater = Timeline()

    fun initialize() {
        parentView.children.removeAll(searchView, loadingView)

        // Add listeners for when the weather info changes
        initializeWeatherInfoChanges()

        // show user's current weather information
        onStartWeatherView()

        // listen for user entry for location changes
        initializeLocationChangeListener()

        // start the small time display
        initializeTimeDisplay()

        updateInterval.items.forEach { menuItem ->
            menuItem.onAction = EventHandler<ActionEvent> {
                updateInterval.text = "Update Every ${menuItem.text}"
            }
        }
    }


    fun animateBackground() {
    }

    /**
     * Called to attempt to populate the
     */
    private fun onStartWeatherView() {
        showLoadingView()
        UserLocationService.getUserLocation().thenComposeAsync {
            when (it) {
                is UserLocation -> getWeatherForUser("${it.city},${it.countryCode}")
                else -> CompletableFuture.completedFuture(WeatherInfo.EMPTY_WEATHER_INFO)
            }
        }.thenApplyAsync {
            Platform.runLater {
                weatherInfoProperty.value = it
                hideLoadingView()
            }
        }.exceptionally { err ->
            hideLoadingView()
            println(err.message)
            err.printStackTrace()
        }
    }

    @FXML
    private fun showSearchView() {
        JFXUtilities.runInFX {
            parentView.children.add(searchView)
            searchAnimation.also { it.rate = 1.0 }.playFromStart()
            searchAnimation.onFinished = EventHandler {  }
        }
    }

    private fun hideSearchView() {
        JFXUtilities.runInFX {
            searchAnimation.also { it.rate = -1.0 }.play()
            searchAnimation.onFinished = EventHandler {
                parentView.children.remove(searchView)
            }
        }
    }

    private fun showLoadingView() {
        JFXUtilities.runInFX{
            parentView.children.add(loadingView)
            loadingAnimation.also { it.rate = 1.0 }.playFromStart()
            loadingAnimation.onFinished = EventHandler {  }
        }
    }

    private fun hideLoadingView() {
        JFXUtilities.runInFX {
            loadingAnimation.also { it.rate = -1.0 }.play()
            loadingAnimation.onFinished = EventHandler {
                parentView.children.remove(loadingView)
            }
        }

    }


    /**
     * transitions need to be smooth (oh yea!)
     */
    private fun createViewTransitionAnimation(view: Node): Animation {
        val animateShowView = FadeTransition(Duration.millis(250.0), view).apply {
            fromValue = 0.0
            toValue = 0.9
            interpolator = Interpolator.EASE_IN
            onFinished = EventHandler { locationSearchField.requestFocus() }
        }

        val animateHideWeatherView = FadeTransition(Duration.millis(100.0), weatherView).apply {
            fromValue = 1.0
            toValue = 0.0
            interpolator = Interpolator.EASE_BOTH
        }

        return ParallelTransition(animateShowView, animateHideWeatherView)
    }

    /**
     * set up event handlers to take care of when the user
     * enters a new location
     */
    private fun initializeLocationChangeListener() {
        locationSearchField.onKeyPressed = EventHandler {
            when (it.code) {
                KeyCode.ESCAPE -> hideSearchView()
                KeyCode.ENTER -> if (locationSearchField.text.isNotBlank()) {

                    hideSearchView()
                    showLoadingView()
                    getWeatherForUser(locationSearchField.text).thenAccept {
                        Platform.runLater {
                            weatherInfoProperty.value = it
                            hideLoadingView()
                        }
                    }.exceptionally { err ->
                        println(err.message)
                        err.printStackTrace()
                        hideLoadingView()
                        null
                    }
                }
                else -> { }
            }
        }
    }

    /**
     * Starts the time display as well as listeners
     */
    private fun initializeTimeDisplay() {
        currentTimeUpdater.keyFrames.clear()
        currentTimeUpdater.keyFrames += listOf(
            KeyFrame(Duration.ZERO, EventHandler {
                currentTime.text = formatCurrentTime()
            }),
            KeyFrame(Duration.ONE)
        )
        currentTimeUpdater.cycleCount = Timeline.INDEFINITE
        currentTimeUpdater.play()

        currentTime.onMousePressed = EventHandler {
            timeFormatProperty.value = when(currentTime.text.contains("(A.M.|P.M.)$".toRegex(RegexOption.IGNORE_CASE))) {
                true -> DateTimeFormatter.ISO_LOCAL_TIME
                false -> DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)
            }
        }
    }

    /**
     * sets up listeners for when the weather information changes
     */
    private fun initializeWeatherInfoChanges() {
        weatherInfoProperty.addListener { _, _, newValue ->
            weatherDescription.text = newValue.description.capitalizeWords()
            weatherIcon.image = Image(URL(newValue.icon).toExternalForm(), true)
            temperature.text = "${if (newValue.temperature.isNaN()) "--" else "${newValue.temperature.roundToInt()}"}°C"
            feelsLikeTemperature.text = "${if (newValue.feelsLike.isNaN()) "--" else "${newValue.feelsLike.roundToInt()}"}°C"
            windSpeed.text = "${if (newValue.windSpeed.isNaN()) "--" else "${newValue.windSpeed}"} KM/H"
            cityLabel.text = newValue.locationName
        }
    }

    /**
     * Formats the current time using the [[timeFormatProperty]]
     * @return String The formatted time
     */
    private fun formatCurrentTime(): String =
        LocalTime.now().withNano(0).format(timeFormatProperty.value)

    /**
     * Given a location entered by the user, we sanitize it and return the tuple location
     * consisting of the city and the country code
     * @param location String raw location entered by the user
     * @return Array<String> A tuple
     */
    private fun sanitizeLocation(location: String): Array<String> {
        return arrayOf(location.dropLast(2).trim { !it.isLetterOrDigit() }, location.takeLast(2).trim())
    }

    /**
     * Finds the city id associated with the given city name and country code
     * @param city String The city name
     * @param countryCode String The 2-letter country code
     * @return City The city object containing the id
     */
    private fun findCityId(city: String, countryCode: String): City {
        return CityIDSearchService.searchForCityID(city, countryCode)
    }

    /**
     * Given the user's input, attempt to find the weather information for the user
     * @param location String The location entered by the user
     * @return CompletableFuture<WeatherInfo>
     */
    private fun getWeatherForUser(location: String): CompletableFuture<WeatherInfo> {
        val (city, countryCode) = sanitizeLocation(location)
        return CompletableFuture.supplyAsync {
            findCityId(city, countryCode)
        }.thenApply {
            OpenWeatherApi.getWeatherDataForCityID(it.id)
        }
    }
}

